%
% These examples are based on the package documentation:
% http://www.ctan.org/tex-archive/macros/latex/contrib/minted
%
\documentclass{article}
    \usepackage[spanish]{babel}
    \usepackage[T1]{fontenc}
    \usepackage[utf8]{inputenc}
    \usepackage{lmodern}
    \usepackage{microtype}
    \usepackage{hyperref}
    \usepackage{minted}

    \begin{document}

    \title{Opty: control concurrencia optimista\footnote{Adaptado al español del material original de Johan Montelius (\url{https://people.kth.se/~johanmon/dse.html})}}
    \author{
        Federico C. Repond \\ {\ttfamily frepond@unq.edu.ar}
        \and
        Esteban Dimitroff Hódi \\ {\ttfamily esteban.dimitroff@unq.edu.ar}
    }
    \maketitle

    \section*{Introducción}
    En este ejercicio vamos a implementar un servidor de transacciones utilizando control de concurrencia optimista. Vamos a aprender como utilizar una estructura de datos actualizable en Erlang que puede ser accedido por procesos concurrentes, posiblemente distribuidos. Antes de empezar demos conocer como funciona el control de concurrencia optimista con validación hacia atrás (backwards validation).
    % In this session you will implement a transaction server using optimistic con- currency control. You will also learn how to implement a updatable data structure in Erlang that can be accessed by concurrent, possibly distributed, processes. Before you start you should know how optimistic concurrency control with backwards validation works.

    \section{La Arquitectura}
    La arquitectura consiste en un servidor con acceso a un store y a un validator. El store está formado por un conjunto de entradas, cada una manteniendo una referencia única. La referencia es actualizada en cada operación de escritura así podemos validar que la entrada no se modificó desde la última vez que la leímos.
    % The architecture consist of a server having access to a store and a validator. The store consist of a set of entries, each holding a value and a unique ref- erence. The reference is updated in each write operation so we can validate that nothing has happened with the entry since we last read its value.

    Un cliente empieza una transacción creando un handler de transacción que le da acceso al store y al proceso de validación. El servidor de transacciones no se involucra en la transacción, simplemente es un proceso por medio del cual obtenemos acceso al store y al proceso de validación.
    % A client starts a transaction by creating a transaction handler that is given access to the store and the validator process. The transaction server is thus not involved in the transaction; it is simply a process from which we can get access to the store and the validator process.

    \subsection{El Handler de Transacciones}
    El handler de transacciones (\texttt{handler}) va a manejar los pedidos de lectura y escritura desde un cliente y cerrar la transacción. En cada operación de lectura el handler mantiene registro de la referencia única de la entrada leída. También mantiene la operación de escritura en un store local de forma tal que el store real no sea modificado hasta que cerremos la transacción.
    % The transaction handler will handle read and write requests from the client and will also to close the transaction. In each read operation the handler keeps track of the unique reference of the entry that is read. It also keeps the write operation in a local store so that the real store is not modified until we want to close the transaction.

    Cuando la transacción se cierra el handler envía los conjuntos de lectura y escritura al validator.
    % When the transaction is to be closed the handler send the read and write sets to the validator.

    \subsection{El Validator}
    El proceso de validación va a ser capaz de decir si un valor de una entrada fue cambiado desde que la transacción leyó la entrada. Si ninguna de las entradas cambió la transacción puede hacer commit y las operaciones de escritura asociadas llevadas a cabo.
    % The validation process will be able to tell if a value of an entry has been changes since the transaction read the entry. If non of the entries have changed the transacting can commit and the associated write operations are performed.

    Dado que hay un solo validator en el sistema, el mismo es l único proceso que escribe algo en el store.
    % Since there is only one validator in the system the validator is the only process that actually writes anything to the store.

    \section{La Implementación}

    Dado que las estructuras de datos en Erlang son inmutables (no podemos cambiar su valor), necesitamos hacer un truco. El store va a ser representado como una tupla de identificadores de procesos. Cada proceso es una entrada y podemos cambiar su valor enviando mensajes \texttt{write}.
    %Since data structures in Erlang are not mutable (we can not change their values), we need to do a trick. The store is represented by a tuple of process identifiers. Each process is an entry and we can change its value by sending it write messages.

    Un handler de transacciones recibe una copia de la tupla cuando es creado pero los procesos que representan entradas por supuesto no son copiadas. El store entonces puede ser compartido por varios handlers de transacciones, todos los que envían un mensaje \texttt{read} a las entradas.
    % A transaction handler is given a copy of the tuple when created but the processes that represent the entries are of course not copied. The store can thus be shared by several transaction handlers, all who can send read messages to the entries.

    El validator no necesita acceder a todo el store dado que reciben los conjuntos de lecturas y escrituras desde los handlers de transacciones. Estos conjuntos contienen los identificadores de procesos de las entradas relevantes.
    % The validator will not need access to the whole store since it will be given the read and write sets from the transaction handlers. These sets will contain the process identifiers of the relevant entries.

    \subsection{Una Entrada}
    Un proceso que implementa una entrada debe tener un valor y una referencia única como estado. Vamos a llamar a esta referencia más adelante ``timestamp'' pero no tiene nada que ver con tiempo, simplemente va a ser una referencia única. La referencia va a ser dada al que lee el valor de forma tal que el validator pueda determinar después si el valor fue modificado. Podríamos hacerlo sin la referencia pero tiene sus ventajas.

    % A process that implements an entry should only have a value and a unique reference as its state. We will below call this reference “time stamp” but it has nothing to do with time, it’s simply a   unique reference. The reference will be given to a reader of the value so that the validator later can determine if the value has been changed. We could do without the reference but it has its advantages.

    \begin{minted}{erlang}
    -module(entry).
    -export([new/1]).

    new(Value) ->
        spawn_link(fun() -> init(Value) end).

    init(Value) ->
        entry(Value, make_ref()).
    \end{minted}

    Notar que estamos usando la primitiva \texttt{spawn\_link/1} para asegurarnos que si el creador de la entrada muere, entonces la entrada también muere.
    %Note that we are using the primitive spawn link/1. This is to ensure that if the creator of the entry dies, then the entry should also die.

    La entrada debe manejar 3 mensajes:
    % Three messages should be handled by an entry:

    \begin{itemize}
      \item \texttt{{read, Ref, Handler}}: un pedido de un handler tageado con una referencia. Debemos devolver un mensaje tageado con una referencia de forma tal que el handler pueda identificar el mensaje correcto. La respuesta va a contener el identificador de proceso de la entrada, el valor y el timestamp actual.
    % {read, Ref, Handler}: a read request from a handler tagged with a reference. We will return a message tagged with the reference so that the handler can identify the correct message. The reply will contain the process identifier of the entry, the value and the current time stamp.
      \item \texttt{{write, Value}}: cambia el valor actual de la entrada, no requiere una respuesta. El timestamp de la entrada es actualizado.
    % {write, Value}: changes the current value of the entry, no reply needed. The time stamp of the entry will be updated.
      \item \texttt{{check, Ref, Read, Handler}}: verifica si el timestamp de la entrada desde que leímos el valor cambio. La respuesta es tageada con la referencia, va a indicar al handler si el timestamp es todavía el mismo o si tiene que abortar.
    % {check, Ref, Read, Handler}: check if the time stamp of the entry has changed since we read the value (at time Read). A reply, tagged with the reference, will tell the handler if the time stamp is still the same or if it has to abort.
      \item \texttt{stop}: termina.
    % stop: terminate.
    \end{itemize}

    Hablamos del handler y no del cliente, esto va a ser claro más adelante. ¿Por qué queremos que el mensaje \texttt{read} incluya el identificador de proceso? No es claro en este momento pero vamos a ver que simplifica escribir un handler de transacciones asincrónico.
    % We here talk about the handler and not the client, this will be clear later. Why do we want the read message to include the process identifier? Not quite clear at the moment but you will see that it makes it easy to write an asynchronous transaction handler.

    \begin{minted}{erlang}
    entry(Value, Time) ->
        receive
            {read, Ref, Handler} ->
                Handler ! {Ref, self(), Value, Time},
                entry(Value, Time);
            {check, Ref, Read, Handler} ->
                if
                    Read == Time ->
                        Handler ! {Ref, ok};
                    true ->
                        Handler ! {Ref, abort}
                end,
                entry(Value, Time);
            {write, New} ->
                entry(New, make_ref());
            stop ->
                ok
        end.
    \end{minted}

    \subsection{El Store}
    Vamos a ocultar la representación del store y proveer solo una API para crear un nuevo store y buscar una entrada. La creación de una tupla se hace simplemente creando una lista de los identificadores de procesos primero y convirtiéndolos en tuplas.
    % We will hide the representation of the store and provide only an API to create a new store and to look-up an entry in the store. Creating a tuple is done simply by first creating a list of all process identifiers and then turning it into a tuple.

    \begin{minted}{erlang}
    -module(store).
    -export([new/1, stop/1, lookup/2]).

    new(N) ->
        list_to_tuple(entries(N, [])).

    stop(Store) ->
        lists:map(fun(E) -> E ! stop end, tuple_to_list(Store)).

    lookup(I, Store) ->
        element(I, Store). % this is a builtin function

    entries(N, Sofar) ->
        if
            N == 0 ->
                Sofar;
            true ->
                Entry = entry:new(0),
                entries(N - 1, [Entry | Sofar])
        end.
    \end{minted}

    \subsection{El Handler de Transacciones}
    Un cliente nunca debe acceder el store directamente. Va a realizar todas las operaciones por medio de un handler de transacciones. Un handler de transacciones se crea para un cliente específico y mantiene el store y el identificador de proceso del proceso validator.
    % A client should never access the store directly. It will perform all operations through a transaction handler. A transaction handler is created for a specific client and holds the store and the process identifier of the validator process.

    Vamos a implementar el handler de forma tal que el cliente pueda hacer \texttt{read} asincrónicos al store. Si la latencia es alta no tiene sentido esperar que termine una operación \texttt{read} antes de empezar una segunda operación.
    % We will implement the handler so that a client can make asynchronous reads to the store. If latencies are high there is no point in waiting for one read operation to complete before initiating a second operation.

    La tarea del handler de transacciones es registrar todas las operaciones de lectura (y cuando se hicieron) y hacer las transacciones de escritura solo visibles al store local. Para hacer esto el handler va a mantener 2 conjuntos: el conjunto de lecturas (\texttt{Read}) y el conjunto de escrituras (\texttt{Write}). El conjunto de lecturas es una lista de tuplas \texttt{{Entry, Time}} y el conjunto de escrituras es una lista de tuplas \texttt{{N, Entry, Value}}. Cuando es el momento de hacer commit, el handler envía los conjuntos de lectura y escritura al validator.
    % The task of the transaction handler is to record all read operations (and at what time these took place) and make write operations only visible in a local store. In order to achieve this the handler will keep two sets: the read set (Reads) and the write set (Writes). The read set is a list of tuples {Entry, Time} and the write set is a list of tuples {N, Entry, Value}. When it is time to commit, the handler sends the read and write sets to the validator.

    Cuando el handler se crea se linkea con su creador. Esto significa que si alguno muere entonces ambos mueren. Esto suena fuerte pero se va a explicar cuando implementemos el servidor.
    % When the handler is created it is also linked to its creator. This means that if one dies both die. This sounds hard but it will be explained once we implement the server.

    \begin{minted}{erlang}
    -module(handler).
    -export([start/3]).

    start(Client, Validator, Store) ->
        spawn_link(fun() -> init(Client, Validator, Store) end).

    init(Client, Validator, Store) ->
        handler(Client, Validator, Store, [], []).
    \end{minted}

    La interfaz del handler es la siguiente:
    % The message interface to the handler is as follows:

    \begin{itemize}
      \item \texttt{{read, Ref, N}}: un pedido de lectura de un cliente  contiene una referencia que debemos usar en el mensaje de respuesta. Un entero \texttt{N} que el índice de la entrada en el store. El handler debe primero mirar el conjunto de escrituras y ver si la entrada \texttt{N} ha sido escrita. Si no encontramos una operación de escritura se envía un mensaje al proceso de la N-ésima entrada del store. Esta entrada va a contestar al handler dado que debemos registrar el tiempo de lectura.
      % {read, Ref, N}: a read request from the client containing a reference that we should use in the reply message. The integer N is the index of the entry in the store. The handler should first look through the write set to see if entry N has been written. If no matching operation is found a message is sent to the nth entry process in the store. This entry will reply to the handler since we need to record the read time.
      \item \texttt{{Ref, Entry, Value, Time}}: una respuesta de una entrada debe ser reenviada al cliente. La entrada y el tiempo se guarda en el conjunto de lecturas del handler. La respuesta al cliente es \texttt{{Ref, Value}}.
      % {Ref, Entry, Value, Time}: a reply from an entry that should be forwarded to the Client. The entry and time is saved in the read set of the handler. The reply to the client is {Ref, Value}.
      \item \texttt{{write, N, Value}}: un mensaje de escritura de un cliente. El entero \texttt{N} es el índice de la entrada en el store y \texttt{Value} el valor. La entrada con índice \texttt{N} y el valor son guardados en el conjunto de escrituras del handler.
      % {write, N, Value}: a write message from the client. The integer N is the index of the entry in the store and Value, the new value. The entry with index N and the value is saved in the write set of the handler.
      \item \texttt{{commit, Ref}}: un mensaje de commit del cliente. Este es el momento de contactar el validator y ver si hay algún conflicto en nuestro conjunto de lectura. Si no lo hay, el validator va a efectuar las operaciones de escritura en el conjunto de escrituras y responder al cliente.
      % {commit, Ref}: a commit message from the client. This is the time to contact the validator and see if there are any conflicts in our read set. If not, the validator will perform the write operations in the write set and reply directly to the client.
    \end{itemize}

    Este es el esqueleto del handler.
    % Here is some skeleton code for the handler.

    \begin{minted}{erlang}
    handler(Client, Validator, Store, Reads, Writes) ->
        receive
            {read, Ref, N} ->
                case lists:keysearch(N, 1, Writes) of
                    {value, {N, _, Value}} ->
                          :
                        handler(Client, Validator, Store, Reads, Writes);
                    false ->
                          :
                          :
                        handler(Client, Validator, Store, Reads, Writes)
                end;
            {Ref, Entry, Value, Time} ->
                  :
                handler(Client, Validator, Store, [...|Reads], Writes);
            {write, N, Value} ->
                Added = [{N, ..., ...}|...],
                handler(Client, Validator, Store, Reads, Added);
            {commit, Ref} ->
                Validator ! {validate, Ref, Reads, Writes, Client};
            abort -> ok
        end.
    \end{minted}

    \subsection{La Validación}
    El validator es responsable de hacer la validación final de las tansacciones. La tarea se hace bastante fácil dado que solo una transacción es válida al mismo tiempo. No hay operaciones concurrentes que pueden entrar en conflicto con el proceso de validación.
    % The validation handler is responsible of doing the final validation of trans- actions. The task is made quite easy since only one transaction is validated at a time. There are no concurrent operations that could possibly conflict with the validation process.

    Cuando iniciamos el validator lo linkeamos también al proceso que lo crea. Esto es para asegurarnos de no dejar ningún proceso zombie.
    % When we start the validator we also link it to the processes that creates it. This is to ensure that we don’t have any zombie processes.

    \begin{minted}{erlang}
    -module(validator).
    -export([start/0]).

    start() ->
        spawn_link(fun() -> init() end).

    init()->
        validator().
    \end{minted}

    El validator recibe pedidos desde un cliente que tiene todo lo necesario para validar que la transacción puede realizar todas las operaciones de escritura que van a ser el resultado de la transacción. El pedido contiene:
    % The validator receives a request from a client containing everything that is needed both to validate that the transaction is allowed and to perform the write operations that will be a result of the transaction. The request contains:

    \begin{itemize}
      \item \texttt{Ref}: una referencia única que taggea el mensaje de respuesta.
    % Ref: a unique reference to tag the reply message.
      \item \texttt{Reads}: un lista de operaciones de lectura que fueron realizadas. El validator debe asegurarse que las entradas del conjunto de lecturas no fue cambiado.
    % Reads: a list of read operations that have been performed. The val- idator must ensure that the entries of the read operations have not been changed.
      \item \texttt{Writes}: la operaciones pendientes de escritura, que si la transacción es válida, se aplicaran al store.
    % Writes: the pending write operations that, if the transaction is valid, should be applied to the store.
      \item \texttt{Client}: el identificador del cliente que quien debemos responder.
    % Client: the process identifier of the client to whom we should return the reply.
    \end{itemize}

    La validación entonces es simplemente chequear que las operaciones de lectura todavía son válidas y si es así actualizar el store con las operaciones de escritura pendientes.
    % Validation is thus simply checking if read operations are still valid and if so update the store with the pending write operations.

    \begin{minted}{erlang}
    validator() ->
        receive
            {validate, Ref, Reads, Writes, Client} ->
                case  validate(Reads) of
                    ok ->
                        update(Writes),
                        Client ! {Ref, ok};
                    abort ->
                        Client ! {Ref, abort}
                end,
                validator();
            _Old ->
                validator()
        end.
    \end{minted}

    Dado que cada operación de lectura es representado como una tupla \texttt{{Entry, Time}} el validator solo necesita enviar un mensaje a la entrada para asegurarse que el timestamp actual de la entrada es el mismo.
    % Since a read operation is represented with a tuple {Entry, Time} the validator need only send a check message to the entry and make sure that the current time-stamp of the entry is the same.

    \begin{minted}{erlang}
    validate(Reads) ->
        {N, Tag} = send_checks(Reads),
        check_reads(N, tag).
    \end{minted}

    Para una mejor performance el validator puede enviar primero los mensajes para chequear todas las entradas y luego recolectar las respuestas. Ni bien una de las entradas responde con un mensaje de \texttt{abort}, terminamos, Notar sin embargo, que debemos ser cuidadosos de que no estamos recibiendo mensajes que pertenecen a una validación previa. Cuando enviamos un el pedido de chequeo debemos taggearlos con una referencia única así sabemos que estamos analizando las respuestas correctas.
    % For better performance the validator can first send check messages to all entries and then collect the replies. As soon as one entry replies with an abort message, we’re done. Note however, that we must be careful so that we are not seeing replies that pertain to a previous validation. When we send our check request we therefore tag them with a unique reference so that we know that we’re counting the right replies.

    \begin{minted}{erlang}
    send_checks(Reads) ->
        Tag = make_ref(),
        Self = self(),
        N = length(Reads),
        lists:map(fun({Entry, Time}) ->
                          Entry ! {check, Tag, Time, Self}
                  end,
                  Reads),
        {N, Tag}.
    \end{minted}


    Recolectar las respuestas es simple, solo debemos contemplar qye no recolectamos una respuesta vieja.

    \begin{minted}{erlang}
    check_reads(N, Tag) ->
        if
            N == 0 ->
                ok;
            true ->
                receive
                    {Tag, ok} ->
                        check_reads(N - 1, Tag);
                    {Tag, abort} ->
                        abort
                end
        end.
    \end{minted}

    Los mensajes viejos que queden encolados deben ser removidos de alguna forma, por eso es muy importante, que el loop principal del validator incluya una cláusula para capturar todo.
    % Old messages that are still in the queue must be removed somehow, this is why, and this is very important, the main loop of the validator includes a catch all clause.

    \subsection{El Servidor}
    Ahora que tenemos todas las piezas para construir el servidor de transacciones. El servidor va a construir el store y un proceso validator. Los clientes pueden entonces abrir una transacción y cada transacción va a recibir un nuevo \texttt{handler} que realizará la tarea.
    %We now have all the pieces to build the transaction server. The server will construct the store and one validator process. Clients can then open a trans- action and each transaction will be given a new handler that is dedicated to the task.

    \begin{minted}{erlang}
    -module(server).
    -export([start/1, open/1, stop/1]).

    start(N) ->
        spawn(fun() -> init(N) end).

    init(N) ->
        Store = store:new(N),
        Validator = validator:start(),
        server(Validator, Store).
    \end{minted}

    El servidor va a esperar pedidos de los clientes y hacer \texttt{spawn} de un nuevo handler y validator de transacciones. Sin embargo hay una trampa aquí en la que no queremos caer. Si el servidor crea el handler de transacciones debemos hacer que el handler sea independiente del servidor (no linkeado). De esta forma si el handler muere no queremos que el servidor muera. Por otro lado, si el cliente muere queremos que el handler muera. La solución es dejar que el cliente cree el handler. Es conveniente para la prueba implementar un cliente que tenga las operaciones: \texttt{read}, \texttt{write}, \texttt{commit}, \texttt{abort} y quizás una \texttt{state} extendiendo el handler para que nos devuelva el estado para analizar.
    %The server could simply wait for requests from clients and spawn a new transaction handler. There is however a trap here that we don’t want to fall into. If the server creates the transaction handler we must let the handler be independent from the server (not linked). If the handler dies we don’t want the server to die. On the other hand, if the client dies we do want the handler to die. The solution is to let the process of the client create the handler.

    \begin{minted}{erlang}
    open(Server) ->
        Server ! {open, self()},
        receive
            {transaction, Validator, Store} ->
                handler:start(self(), Validator, Store)
        end.
    \end{minted}


    Esto tiene implicancias también en donde corre el handler de transacciones- Algo que vamos a discutir cuando terminemos con el servidor.
    %This will also have implications on where the transaction handler is running. Something that we might discuss further when we are done with the server.

    El servidor ahora es casi trivial.
    %The server now becomes almost trivial.

    \begin{minted}{erlang}
    server(Validator, Store) ->
        receive
            {open, Client} ->
                 :
                server(Validator, Store);
            stop ->
                 store:stop(Store)
        end.
    \end{minted}

    Listo, implementamos un servidor de transacciones con control de concurrencia optimista!
    %You’re done, you have implemented a transaction server using optimistic concurrency control.

    \section{Performance}
    ¿Performa? ¿Cuántas transacciones podemos hacer por segundo? ¿Cuáles son las limitaciones en el número de transacciones concurrentes y la tasa de éxito? Esto por supuesto depende del tamaño del store, cuántas operaciones de write hace cada transacción, cuanto tiempo tenemos entre las instrucciones de read y el commit final. ¿Algo más?

    %Does it perform? How many transaction can we do per second? What are the limitations on the number of concurrent transactions and the success rate? This does of course depend on the size of the store, how many write operations each transaction does and how much delay we have in between the read instructions of the transaction and the final commit instruction.

    Algunas preguntas sobre Erlang también son interesantes plantear. ¿Es realista la implementación del store que tenemos? Independientemente del handler de transacciones, ¿qué rápido podemos operar sobre el store? ¿Qué sucede si hacemos esto en una red de Erlang distribuida, qué es lo que se copia cuando el handler de transacciones arranca? ¿Dónde corre el handler? ¿Cuáles son los pros y contras de la estrategia de implementación?

    %Some Erlang related questions can be fair to raise. Is it realistic with the store implementation that we have? Regardless of the transaction handler, how fast can we operate on the store? What happens if we run this in a distributed Erlang network, what is actually copied when a transaction handler is started? Where is the handler running? Pros and cons of this implementation strategy?
    \end{document}
